package com.ffdc.mediascreen.controller.view;

import com.ffdc.mediascreen.model.entity.Note;
import com.ffdc.mediascreen.model.entity.PatientDTO;
import com.ffdc.mediascreen.model.entity.Risk;
import com.ffdc.mediascreen.model.repository.RepositoryManager;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.event.RowEditEvent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@ViewScoped
@Named
@Slf4j
public class PatientBean implements Serializable {

    /**
     * Repository entry point
     */
    @Autowired
    private RepositoryManager repositoryManager;

    /**
     * Notes of the patient selected
     */
    @Getter
    private List<Note> notes;
    /**
     * List used for list all patient
     */
    @Getter
    private List<PatientDTO> patients;

    @Getter
    @Setter
    private Note noteInput;

    /**
     * field used for filtering the list of patient
     */
    @Getter
    @Setter
    private String inputLastNameSearched;

    /**
     * Patient used for form to add a user
     */
    @Getter
    @Setter
    private PatientDTO userInputPatient;

    /**
     * Primefaces calendar component doesn't not support LocalDate yet
     */
    @Getter
    @Setter
    private String birthDateUserInput;



    /**
     * Used for display the note of a selected patient
     * Reset in scope creation
     * Updated in {@link #loadNotes(PatientDTO)} ()}
     */
    @Getter
    private Boolean displayNotes;

    /**
     * Patient selectd by user
     */
    private PatientDTO patientSelected;

    /**
     * Used for display the new Patient form
     */
    @Getter
    private Boolean displayNewPatientForm;

    /**
     * Used for display the new note Form
     */
    @Getter
    private Boolean displayNewNoteForm;

    /**
     * Used for display the button for new Note
     */
    @Getter
    private Boolean displayNewNoteInput;

    /**
     * Used as cache for edit a Note
     */
    @Getter
    @Setter
    private Note noteToEdit;

    @Getter
    private Boolean displayNoteToEditForm;

    @Getter
    private Risk repportSelected;

    @Getter
    private String listOfTriggersFromRapportSelected;

    @PostConstruct
    public void setup() {
        resetListPatient();
        resetUserInput();
        notes = new ArrayList<>();
        resetDisplay();
        noteToEdit = new Note();
    }

    public void resetDisplay() {
        displayNotes = false;
        displayNewPatientForm = false;
        displayNewNoteForm = false;
        displayNewNoteInput = false;
        displayNoteToEditForm = false;
    }

    public void displayHideNewPatient() {
        displayNewPatientForm = !displayNewPatientForm;
    }


    /**
     * Use for send binding Patient {@link #userInputPatient} to the entityController
     */
    public void addPatient() {

        patients.add(repositoryManager.getPatientRepository().createPatient(userInputPatient));
        resetUserInput();
    }

    /**
     * Used for send a patient updated in a datable to the entityController
     *
     * @param event
     */
    public void editPatient(RowEditEvent event) {
        PatientDTO patient = (PatientDTO) event.getObject();
        Optional.ofNullable(patient)
                .filter(myPatient -> !myPatient.getId().isEmpty() && !myPatient.getId().isBlank())
                .ifPresentOrElse(myPatient ->
                            repositoryManager.getPatientRepository().updatePatient(myPatient)

                        , () -> new FacesMessage(FacesMessage.SEVERITY_WARN, "Update Fail", "Impossible to update the user"));

    }


    /**
     * Used for clean all input to Patient view
     */
    private void resetUserInput() {
        userInputPatient = new PatientDTO();
     //   birthDateUserInput = new Date();
    }

    /**
     * Reload the patients
     */
    private void resetListPatient() {
        patients = repositoryManager.getPatientRepository().getAllPatients();
    }

    /**
     * Method call by Ajax event in input header of patient list
     * Used for filter the patient list
     */
    public void filteringPatients() {
        Optional.ofNullable(inputLastNameSearched)
                .filter(lastName -> !lastName.isBlank())
                .ifPresentOrElse(lastName -> patients = repositoryManager.getPatientRepository().filterPatientList(lastName),
                        this::resetListPatient);
    }

    //
    // ############ NOTES #############################
    //

    /**
     * Fetch all notes from the patient selected
     * In list of note {@link #notes}
     */
    public void loadNotes(PatientDTO patient) {
        displayNewNoteForm = false;
        displayNoteToEditForm = false;
        notes = new ArrayList<>();
        patientSelected = patient;
        Optional.of(repositoryManager.getNoteRepository().getNoteByUserId(patient.getId()))
                .filter(notesFromService -> !notesFromService.isEmpty())
                .ifPresentOrElse(notes::addAll,
                        () -> {
                            logger.info("No notes found for " + patient);
                            FacesContext.getCurrentInstance()
                                    .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "No notes found", "The patient " + patient.getLastName() + " " + patient.getFirstName() + " hasn't notes"));
                        });
        displayNotes = notes.size() > 0;

        displayNewNoteInput = Optional.of(patient).map(PatientDTO::getId).isPresent();
    }


    public void loadNewNote() {
        displayNewNoteForm = !displayNewNoteForm;
        resetNoteInput();

    }

    public void resetNoteInput() {
        noteInput = new Note();
        noteInput.setPatientId(patientSelected.getId());
    }

    public void createNote() {
        noteInput.setDateOfInput(LocalDateTime.now());
        notes.add(repositoryManager.getNoteRepository().createNote(noteInput));
        displayNewNoteForm = false;
    }

    public void deleteNote(Note note) {

        repositoryManager.getNoteRepository().deleteNote(note.getId());
        notes.remove(note);
    }

    public void loadEditNote(Note note) {
        noteToEdit = Optional.ofNullable(note).orElseThrow();
        displayNoteToEditForm = true;
    }

    public void updateNote() {
        Note newNote = repositoryManager.getNoteRepository().updateNote(noteToEdit);
        notes.remove(newNote);
        notes.add(newNote);
    }

    public void resetEditNote() {
        displayNoteToEditForm = false;
    }

    public void loadReport(PatientDTO patient) {
        Optional.ofNullable(repositoryManager.getDiabetesRepository().getDiabetesRepport(patient.getId()))
                .ifPresentOrElse(risk ->
                        {
                            repportSelected = risk;
                            listOfTriggersFromRapportSelected = risk.getTriggers().entrySet()
                                    .stream()
                                    .map(entry -> entry.getKey().concat("(").concat(entry.getValue().toString()).concat(")") )
                                    .collect(Collectors.joining("\n"));
                        },
                        () -> {
                            repportSelected = new Risk();
                            listOfTriggersFromRapportSelected = "";
                        });


    }


    public String formatLocalDate(LocalDate date)
    {
        return date.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

}
