package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.Doctor;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface DoctorRepository extends ReactiveMongoRepository<Doctor,String>
{

    Mono<Doctor> findByLogin(String login);

}
