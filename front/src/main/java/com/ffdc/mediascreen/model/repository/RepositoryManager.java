package com.ffdc.mediascreen.model.repository;


public interface RepositoryManager {

    PatientRepository getPatientRepository();

    NoteRepository getNoteRepository();

    DiabetesRepository getDiabetesRepository();
}
