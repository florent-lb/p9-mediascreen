package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.Note;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient("note")
public interface NoteRepository {

    @GetMapping(value = "/notes/{idUser}", produces = APPLICATION_JSON_VALUE)
    List<Note> getNoteByUserId(@PathVariable String idUser);

    @PostMapping(value = "/note",consumes = APPLICATION_JSON_VALUE,produces = APPLICATION_JSON_VALUE)
    Note createNote(@RequestBody Note noteInput);

    @DeleteMapping(value = "/note/{id}")
    void deleteNote(@PathVariable String id);

    @PutMapping(value = "/note",consumes = APPLICATION_JSON_VALUE,produces = APPLICATION_JSON_VALUE)
    Note updateNote(@RequestBody Note noteToEdit);
}
