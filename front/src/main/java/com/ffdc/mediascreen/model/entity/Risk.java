package com.ffdc.mediascreen.model.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
public class Risk implements Serializable
{

    private String level;

    private int ageCalculated;

    private char sex;

    private HashMap<String,Long> triggers;

}
