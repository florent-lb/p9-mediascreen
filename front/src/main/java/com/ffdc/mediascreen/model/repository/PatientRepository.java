package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.PatientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "patient")
public interface PatientRepository
{

    @GetMapping(value = "/list", consumes = APPLICATION_JSON_VALUE,produces = APPLICATION_JSON_VALUE)
    List<PatientDTO> getAllPatients();

    @PostMapping(value = "/patient",consumes = APPLICATION_JSON_VALUE,produces = APPLICATION_JSON_VALUE)
    PatientDTO createPatient(PatientDTO patient);

    @PostMapping(value = "/patient/update",consumes = APPLICATION_JSON_VALUE )
    void updatePatient(PatientDTO patient);

    @GetMapping(value = "/patient/search/{lastName}",produces = APPLICATION_JSON_VALUE)
    List<PatientDTO> filterPatientList(@PathVariable String lastName);
}
