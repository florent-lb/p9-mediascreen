package com.ffdc.mediascreen.model.repository;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class RepositoryManagerImpl implements RepositoryManager {

    @Getter
    @Autowired
    public PatientRepository patientRepository;

    @Getter
    @Autowired
    public NoteRepository noteRepository;

    @Autowired
    @Getter
    public DiabetesRepository diabetesRepository;
}
