package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.Risk;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "diabetes")
public interface DiabetesRepository {

    @GetMapping(value = "/assess", produces = MediaType.APPLICATION_JSON_VALUE)
    Risk getDiabetesRepport(@RequestParam(name = "patientId") String patientId);

}
