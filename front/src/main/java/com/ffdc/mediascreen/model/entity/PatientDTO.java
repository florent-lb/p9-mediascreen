package com.ffdc.mediascreen.model.entity;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class PatientDTO implements Serializable {

    private String id;

    @NotNull
    @NotEmpty(message = "Last name can't be empty")
    private String lastName;
    @NotNull
    @NotEmpty(message = "First name can't be empty")
    private String firstName;

    private Date birthDate;

    private String address;

    private String phone;

    private char sex;


}