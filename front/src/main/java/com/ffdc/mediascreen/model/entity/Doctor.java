package com.ffdc.mediascreen.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "doctor")
@Data
@EqualsAndHashCode(of = "id")
public class Doctor {

    @Id
    private String id;

    private String login;

    private String password;

}
