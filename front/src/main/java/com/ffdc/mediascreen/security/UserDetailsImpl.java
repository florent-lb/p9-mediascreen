package com.ffdc.mediascreen.security;

import com.ffdc.mediascreen.model.entity.Doctor;
import com.ffdc.mediascreen.model.repository.DoctorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Optional;

@Configuration
@Qualifier("mediascreenUserDetails")
public class UserDetailsImpl implements UserDetailsService {

    @Value("${app.admin.name}")
    private String adminName;

    @Value("${app.admin.password}")
    private String adminPassword;

    @Autowired
    private DoctorRepository doctorRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        if(login.equals(adminName))
        {
            return new User(adminName,adminPassword,AuthorityUtils
                    .createAuthorityList("ADMIN"));
        }

        Doctor doc = doctorRepository.findByLogin(login)
                .block();
        return Optional.ofNullable(doc)
                .map(doctor ->
                {
                    List<GrantedAuthority> grantedAuthorities = AuthorityUtils
                            .createAuthorityList("DOCTOR");

                    // The "User" class is provided by Spring and represents a model class for user to be returned by UserDetailsService
                    // And used by auth manager to verify and check user authentication.
                    return new User(doctor.getLogin(), doctor.getPassword(), grantedAuthorities);
                })
                .orElseThrow(() -> new UsernameNotFoundException("No user found with these credentials"));

    }
}
