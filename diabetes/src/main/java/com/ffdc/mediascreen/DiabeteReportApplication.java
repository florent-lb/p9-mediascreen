package com.ffdc.mediascreen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication(scanBasePackages = "com.ffdc.mediascreen")
@EnableWebFlux
@EnableFeignClients(basePackages = "com.ffdc.mediascreen.model.repository")
@EnableReactiveMongoRepositories(basePackages = "com.ffdc.mediascreen.model.repository")
public class DiabeteReportApplication {


    public static void main(String[] args) {
        SpringApplication.run(DiabeteReportApplication.class, args);
    }


}
