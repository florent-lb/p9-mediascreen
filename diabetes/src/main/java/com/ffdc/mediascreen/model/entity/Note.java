package com.ffdc.mediascreen.model.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class Note implements Serializable {

    @NotNull
    private String id;

    @NotNull
    private String patientId;

    @NotNull
    private String text;

    private LocalDateTime dateOfInput;

    private String author;


}
