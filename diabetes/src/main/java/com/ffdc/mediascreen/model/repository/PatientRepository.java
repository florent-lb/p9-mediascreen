package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.PatientDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient(value = "patient")
public interface PatientRepository
{

    @GetMapping(value = "/patient/{id}", consumes = APPLICATION_JSON_VALUE,produces = APPLICATION_JSON_VALUE)
    PatientDTO getPatient(@PathVariable String id);

}
