package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.Trigger;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

import java.util.Optional;

public interface TriggerRepository extends ReactiveMongoRepository<Trigger,String> {
    Mono<Trigger> findByLabel(String labelTriger);
}
