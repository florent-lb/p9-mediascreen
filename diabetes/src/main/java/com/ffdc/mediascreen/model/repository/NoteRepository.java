package com.ffdc.mediascreen.model.repository;

import com.ffdc.mediascreen.model.entity.Note;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.concurrent.Flow;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient("note")
public interface NoteRepository {

    @GetMapping(value = "/notes/{idUser}", produces = APPLICATION_JSON_VALUE)
    List<Note> getNoteByUserId(@PathVariable String idUser);
}
