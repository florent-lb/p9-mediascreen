package com.ffdc.mediascreen.model.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Trigger
{
    @Id
    private String id;

    private String label;
}
