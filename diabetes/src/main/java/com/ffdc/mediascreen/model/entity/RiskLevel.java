package com.ffdc.mediascreen.model.entity;

public enum RiskLevel {

    NONE,
    LIMITED,
    DANGER,
    EARLY
}
