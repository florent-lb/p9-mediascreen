package com.ffdc.mediascreen.controller;

import com.ffdc.mediascreen.model.entity.*;
import com.ffdc.mediascreen.model.repository.NoteRepository;
import com.ffdc.mediascreen.model.repository.PatientRepository;
import com.ffdc.mediascreen.model.repository.TriggerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.ffdc.mediascreen.model.entity.RiskLevel.*;


@Service
@Slf4j
public class RiskControllerImpl implements RiskController {

    @Autowired
    private PatientRepository patientRepository;

    @Autowired
    private NoteRepository noteRepository;

    @Autowired
    private TriggerRepository triggerRepository;

    /**
     * Used for calculated a risk for a patient
     *
     * @param idPatient technical id of a patient
     * @return the risk calculated see {@link RiskLevel}
     */
    @Override
    public Mono<Risk> calculateRisk(String idPatient) throws HttpClientErrorException {

        Risk risk = new Risk();

        return Mono.fromSupplier(() -> Optional.ofNullable(patientRepository.getPatient(idPatient))
                .orElseThrow(() -> new NullPointerException("No patient found")))
                .onErrorStop()
                .map(patient ->
                {
                    //Calculate Age And sex
                    calcAge(patient.getBirthDate()).subscribe(risk::setAgeCalculated);
                    Mono.justOrEmpty(patient.getSex()).subscribe(risk::setSex);
                    return patient;
                })
                .flatMap(patient ->
                        //Calculate Triggers
                        triggerRepository.findAll().collectList().map(triggers -> searchTriggers(triggers, idPatient))
                                .map(triggerLongHashMap ->
                                {
                                    //Apply triggers and calculate level
                                    risk.setTriggers(triggerLongHashMap);
                                    risk.setLevel(calcLevel(triggerLongHashMap, risk.getAgeCalculated(), risk.getSex()));
                                    return risk;
                                }));


    }

    /**
     * Use to count how many triggers appears in patient's notes
     *
     * @param triggers  List of triggers to search in notes
     * @param patientId technical id of the patient (use to found notes)
     * @return Map with in KEY : {@link Trigger} | key : count of the trigger in key
     */

    private HashMap<String, Long> searchTriggers(List<Trigger> triggers, String patientId) {

        HashMap<String, Long> retour = new HashMap<>();
        noteRepository.getNoteByUserId(patientId).forEach(
                note -> {
                    triggers
                            .stream()
                            .filter(trigger -> note.getText().contains(trigger.getLabel()))
                            .forEach(trigger ->
                            {
                                retour.merge(trigger.getLabel(), (long) 1, Long::sum);
                            });
                }
        );
        return retour;
    }

    /**
     * Calulate the age of the patient a T moment where T is {@link LocalDate#now()}
     *
     * @param birthdate of the patient
     * @return the age in year produce with @{@link Period#between(LocalDate birthdate, LocalDate now)}
     */
    private Mono<Integer> calcAge(Date birthdate) {
        return Mono.fromCallable(() ->
                Period.between(LocalDate.ofInstant(Optional.ofNullable(birthdate)
                                .map(date -> ((java.util.Date)date).toInstant())
                                .orElseThrow(() -> new NullPointerException("BirthDate is empty !!")), ZoneId.systemDefault()),
                        LocalDate.now()).getYears());
    }


    /**
     * Determine the level of risk for a patient
     *
     * @param triggers triggers in notes of the patient (key => trigger | nb count)
     * @param age      of the patient
     * @param sex      of the patient
     * @return the {@link RiskLevel}
     * <p>
     * {@link RiskLevel#NONE} :
     * if the patient is not in the following criteria
     * {@link RiskLevel#LIMITED} :
     * if the patient have a least 3 triggers and more than 30 years old
     * {@link RiskLevel#DANGER}
     * if the patient have less 30 years old:
     * if the patient is a man between 4 and 5 triggers
     * or a woman with 4 and 7 triggers
     * <p>
     * if the patient have more than 30 years old :
     * if the patient is a man and more than 6 triggers
     * or a woman and more than 8 triggers
     * <p>
     * {@link RiskLevel#EARLY}
     * if the patient have less 30 years old:
     * if the patient is a man and more than 5 triggers
     * or a woman and more than 7 triggers
     */
    public RiskLevel calcLevel(HashMap<String, Long> triggers, int age, char sex) {
        RiskLevel level = NONE;
        long nbTriggerTotal = triggers.values().stream().mapToLong(value -> value).sum();

        if (age <= 30) {

            if (sex == 'M') {
                if (nbTriggerTotal > 3 && nbTriggerTotal <= 5) {
                    level = DANGER;
                } else if (nbTriggerTotal > 5) {
                    level = EARLY;
                }
            } else if (sex == 'F') {
                if (nbTriggerTotal > 4 && nbTriggerTotal <= 7) {
                    level = DANGER;
                } else if (nbTriggerTotal > 7) {
                    level = EARLY;
                }
            }
        } else {
            if (nbTriggerTotal > 8) {
                level = EARLY;
            } else if (nbTriggerTotal > 6) {
                level = DANGER;
            } else if (nbTriggerTotal > 2) {
                level = LIMITED;
            }
        }
        return level;
    }


}
