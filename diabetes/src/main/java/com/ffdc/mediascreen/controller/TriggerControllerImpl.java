package com.ffdc.mediascreen.controller;

import com.ffdc.mediascreen.model.entity.Trigger;
import com.ffdc.mediascreen.model.repository.TriggerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TriggerControllerImpl implements TriggerController {

    @Autowired
    private TriggerRepository repository;

    @Override
    public Mono<Trigger> addTrigger(Trigger trigger) {
        return repository.findByLabel(trigger.getLabel())
                .switchIfEmpty(repository.insert(trigger));
    }

    @Override
    public Mono<Void> deleteTrigger(String labelTriger) {
        return repository.findByLabel(labelTriger).flatMap(repository::delete);
    }
}
