package com.ffdc.mediascreen.controller;

import com.ffdc.mediascreen.model.entity.Trigger;
import reactor.core.publisher.Mono;

public interface TriggerController {

    Mono<Trigger> addTrigger(Trigger trigger);

    Mono<Void> deleteTrigger(String labelTriger);
}
