package com.ffdc.mediascreen.controller;

import com.ffdc.mediascreen.model.entity.Risk;
import reactor.core.publisher.Mono;

public interface RiskController {
    Mono<Risk> calculateRisk(String idPatient);
}
