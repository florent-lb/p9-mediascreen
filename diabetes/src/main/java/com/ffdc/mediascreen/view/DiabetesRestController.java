package com.ffdc.mediascreen.view;

import com.ffdc.mediascreen.controller.RiskController;
import com.ffdc.mediascreen.controller.TriggerController;
import com.ffdc.mediascreen.model.entity.Risk;
import com.ffdc.mediascreen.model.entity.Trigger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@Slf4j
public class DiabetesRestController {
    @Autowired
    private RiskController riskController;

    @Autowired
    private TriggerController triggerController;

    /**
     * Used to generate a Diabetes Risk object
     *
     * @param idPatient technical id of a patient
     * @return the patient's diabetes risk
     */
    @GetMapping(value = "/assess")
    public Mono<Risk> getRiskById(@RequestParam(name = "patientId") String idPatient) {
        return riskController.calculateRisk(idPatient);
    }

    /**
     * Use to add a new trigger in the list for generate diabetes report
     *
     * @param trigger the trigger to add with null id
     * @return the new trigger created with its id
     */
    @PostMapping(value = "/trigger")
    public Mono<Trigger> addTrigger(@RequestBody Trigger trigger) {
        return triggerController.addTrigger(trigger);
    }

    /**
     * Use to add a new trigger in the list for generate diabetes report
     *
     * @param label the label of the trigger to delete
     * @return the new trigger created with its id
     */
    @DeleteMapping(value = "/trigger/{label}")
    public Mono<Void> deleteTriggerByLabel(@PathVariable String label) {
        return triggerController.deleteTrigger(label);
    }
}
