package com.ffdc.mediascreen.diabetes;

import com.ffdc.mediascreen.controller.RiskControllerImpl;
import com.ffdc.mediascreen.controller.TriggerControllerImpl;
import com.ffdc.mediascreen.model.entity.Note;
import com.ffdc.mediascreen.model.entity.PatientDTO;
import com.ffdc.mediascreen.model.entity.Risk;
import com.ffdc.mediascreen.model.entity.Trigger;
import com.ffdc.mediascreen.model.repository.NoteRepository;
import com.ffdc.mediascreen.model.repository.PatientRepository;
import com.ffdc.mediascreen.model.repository.TriggerRepository;
import com.ffdc.mediascreen.view.DiabetesRestController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = DiabetesRestController.class)
@Import({RiskControllerImpl.class, TriggerControllerImpl.class})
class DiabetesApplicationTests {


    @MockBean
    private TriggerRepository triggerRepository;

    @MockBean
    private PatientRepository patientRepository;

    @MockBean
    private NoteRepository noteRepository;

    @Autowired
    private WebTestClient webTestClient;

    Trigger trigTest;

    @BeforeEach
    public void setup() {
        trigTest = new Trigger();
        trigTest.setLabel("TEST");
    }

    @Test
    @DisplayName("Calc a Risk")
    public void get_whenAsskAnAssess_ShouldReturnARiskRapport() {
        PatientDTO patient = new PatientDTO();
        patient.setBirthDate(Date.from(LocalDate.now().minusYears(20).atStartOfDay(ZoneId.systemDefault()).toInstant()));
        patient.setSex('M');


        Note n1 = new Note();
        n1.setText("TEST");
        n1.setDateOfInput(LocalDateTime.now().minusDays(1));
        Note n2 = new Note();
        n2.setText("TEST");
        n2.setDateOfInput(LocalDateTime.now().minusDays(2));


        when(patientRepository.getPatient(anyString())).thenReturn(patient);
        when(triggerRepository.findAll()).thenReturn(Flux.fromIterable(List.of(trigTest)));
        when(noteRepository.getNoteByUserId(anyString())).thenReturn(List.of(n1, n2));


        webTestClient.get()
                .uri("/assess?patientId=111111")
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(Risk.class);


        verify(patientRepository, times(1)).getPatient(anyString());
        verify(triggerRepository, times(1)).findAll();
        verify(noteRepository, times(1)).getNoteByUserId(anyString());


    }

    @Test
    @DisplayName("Add a new trigger")
    public void post_whenAddAATrigger_ShouldReturnOk() {

        when(triggerRepository.findByLabel(any(String.class))).thenReturn(Mono.empty());
        when(triggerRepository.insert(any(Trigger.class))).thenReturn(Mono.just(trigTest));

        webTestClient.post()
                .uri("/trigger")
                .body(BodyInserters.fromValue(trigTest))
                .exchange()
                .expectStatus()
                .isOk().expectBody(Trigger.class);

        verify(triggerRepository, Mockito.times(1)).findByLabel(any(String.class));
        verify(triggerRepository, Mockito.times(1)).insert(any(Trigger.class));
    }


}
