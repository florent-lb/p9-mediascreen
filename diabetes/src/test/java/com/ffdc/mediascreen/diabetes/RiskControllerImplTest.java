package com.ffdc.mediascreen.diabetes;

import com.ffdc.mediascreen.controller.RiskControllerImpl;
import com.ffdc.mediascreen.model.entity.RiskLevel;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

@Slf4j
public class RiskControllerImplTest {

    private static RiskControllerImpl riskController;


    @BeforeAll
    public static void setup() {
        riskController = new RiskControllerImpl();
    }

    @ParameterizedTest
    @MethodSource("createValuesCalcLevel")
    @DisplayName("Calc Levels")
    public void calcLevel_whenCalcLevel_shouldReturnLimited(long nbTriggers,int age, char sex,RiskLevel levelResult) {

        HashMap<String, Long> triggers = new HashMap<>(Map.of("TRIGGER",nbTriggers));

       RiskLevel level =  riskController.calcLevel(triggers,age,sex);
        logger.info("Calc Level : Triggers : {} | Age : {} | Sex : {} -> {}",nbTriggers,age,sex,levelResult.name());
       Assertions.assertThat(level)
               .isEqualTo(levelResult);

    }


    private static Stream<Arguments> createValuesCalcLevel()
    {
        return Stream.of(
                //LIMITED
                Arguments.of((long)3,50,'M',RiskLevel.LIMITED),
                //DANGER
                Arguments.of((long)4,25,'M',RiskLevel.DANGER),
                Arguments.of((long)5,25,'F',RiskLevel.DANGER),
                Arguments.of((long)7,45,'F',RiskLevel.DANGER),
                Arguments.of((long)7,45,'M',RiskLevel.DANGER),
                //EARLY
                Arguments.of((long)6,25,'M',RiskLevel.EARLY),
                Arguments.of((long)8,25,'F',RiskLevel.EARLY),
                Arguments.of((long)9,45,'F',RiskLevel.EARLY),
                Arguments.of((long)9,45,'M',RiskLevel.EARLY),
                //NONE
                Arguments.of((long)1,50,'M',RiskLevel.NONE),
                Arguments.of((long)3,25,'F',RiskLevel.NONE)
        );
    }

}
