# P9-mediscreen
Software for generate diabetes report, saved patients information and follow theirs treatment with note system.

## Build

On each module : diabetes, note, patient, edge-service, front
### Dev/Test
<code>mvn clean install</code>

Use docker-compose-dev for Mongo database in dev or update application-dev.properties. <br/>
<code>docker-compose -f ~\P9-mediscreen\docker-compose-dev up -d</code>
### production
<code>mvn clean install -P prod</code>

Use docker-compose for Mongo database in production or update application.properties.

## Run
<code>java -jar {project.artifactId}.jar</code><br />
### dev
ex : ~/P9-mediscreen/front/target> java -jar -Dspring.profiles.active=dev front.jar

# Reports
<code>mvn clean verify</code><br />
Go into : ~/P9-mediscreen/{project.artifactId}/site/jacoco -> index.html
