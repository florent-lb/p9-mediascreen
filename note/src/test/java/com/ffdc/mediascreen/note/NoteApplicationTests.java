package com.ffdc.mediascreen.note;

import com.ffdc.mediascreen.note.controller.impl.ControllerManagerImpl;
import com.ffdc.mediascreen.note.controller.impl.NoteControllerImpl;
import com.ffdc.mediascreen.note.model.entities.Note;
import com.ffdc.mediascreen.note.model.repositories.NoteRepository;
import com.ffdc.mediascreen.note.view.NoteRestController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = NoteRestController.class)
@Import({ NoteControllerImpl.class, ControllerManagerImpl.class})
class NoteApplicationTests {

    private Note n1;

    private Note n2;

    private Note n3;

    @MockBean
    private NoteRepository repository;

    @Autowired
    private WebTestClient webTestClient;

    @BeforeEach
    public void setup() {
        n1 = new Note();
        n1.setId("1");
        n1.setPatientId("1");
        n1.setText("Note One!");

        n2 = new Note();
        n2.setId("2");
        n2.setPatientId("2");
        n2.setText("Note Three And not two!");

        n3 = new Note();
        n3.setId("3");
        n3.setPatientId("3");


    }


    @Test
    @DisplayName("List notes of a user")
    public void get_whenaskAllUser_ShouldReturnList() {
        when(repository.findByPatientId(n1.getPatientId())).thenReturn(Flux.just(n1));

        webTestClient.get()
                .uri("/notes/" + n1.getPatientId())
                .exchange()
                .expectStatus()
                .is2xxSuccessful().expectBodyList(Note.class);


        verify(repository, Mockito.times(1)).findByPatientId(n1.getPatientId());

    }

    @Test
    @DisplayName("Add a note on a patient")
    public void post_whenAddANoteOnAPatient_ShouldReturnOk() {


        when(repository.insert(any(Note.class))).thenReturn(Mono.just(n2));

        Note result = webTestClient.post()
                .uri("/note")
                .body(BodyInserters.fromValue(n2))
                .exchange()
                .expectStatus()
                .isOk().expectBody(Note.class)
                .returnResult()
                .getResponseBody();
        Assertions.assertThat(result).extracting(Note::getPatientId).isEqualTo(n2.getPatientId());

        verify(repository, Mockito.times(1)).insert(any(Note.class));
    }


    @Test
    @DisplayName("Add an empty note on a patient")
    public void post_whenAddANoteOnAPatientWithNoText_ShouldReturn422() {

        when(repository.insert(any(Note.class))).thenThrow(ConstraintViolationException.class);

        webTestClient.post()
                .uri("/note")
                .body(BodyInserters.fromValue(n3))
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);

    }


    @Test
    @DisplayName("Delete a note")
    public void delete_whenDeleteANote_ShouldRetunOk() {

        webTestClient.delete()
                .uri("/note/" + n2.getId())
                .exchange()
                .expectStatus()
                .isOk();

        verify(repository, Mockito.times(1)).deleteById(n2.getId());
    }

    @Test
    @DisplayName("Edit a note")
    public void patch_whenUpdateANote_ShouldRetunOk() {

        when(repository.findById(n2.getId())).thenReturn(Mono.just(n2));
        when(repository.save(n2)).thenReturn(Mono.just(n2));

        webTestClient.put()
                .uri("/note")
                .body(BodyInserters.fromValue(n2))
                .exchange()
                .expectStatus()
                .isOk().expectBody(Note.class);

        verify(repository, Mockito.times(1)).findById(n2.getId());
        verify(repository, Mockito.times(1)).save(n2);

    }

    @Test
    @DisplayName("Update unknown note on a patient")
    public void post_whenUpdateAUnknownNote_ShouldReturn422() {

        when(repository.findById(anyString())).thenReturn(Mono.empty());

        webTestClient.put()
                .uri("/note")
                .body(BodyInserters.fromValue(n3))
                .exchange()
                .expectStatus()
                .isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY);

    }


}
