package com.ffdc.mediascreen.note.model.repositories;

import com.ffdc.mediascreen.note.model.entities.Note;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

@Configuration
public interface NoteRepository extends ReactiveMongoRepository<Note,String> {

    Flux<Note> findByPatientId(String patientId);

}
