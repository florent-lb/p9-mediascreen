package com.ffdc.mediascreen.note.view;

import com.ffdc.mediascreen.note.controller.contract.ControllerManager;
import com.ffdc.mediascreen.note.model.dto.NoteDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.ffdc.mediascreen.note.model.entities.Note;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;
import javax.validation.UnexpectedTypeException;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
public class NoteRestController {

    private final ControllerManager controllerManager;

    public NoteRestController(ControllerManager controllerManager) {
        this.controllerManager = controllerManager;
    }

    /**
     * Used to find patient's note
     *
     * @param patientId technical id of the patient
     * @return all notes for this patient
     */
    @GetMapping(value = "/notes/{patientId}", produces = APPLICATION_JSON_VALUE)
    public Flux<Note> getNoteByPatientId(@PathVariable String patientId) {
        return controllerManager.getNoteController().getNoteByPatientId(patientId);
    }

    /**
     * Used to persist a new note
     *
     * @param note Must have the patient ID!
     * @return the new note note with it id
     */
    @PostMapping(value = "/note", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
    public Mono<Note> addNote(@RequestBody NoteDTO note) {
        return controllerManager.getNoteController().createNote(note);

    }

    /**
     * Used to remove a note
     *
     * @param id The unique Id of the note (<strong>NOT</strong> the user id !)
     */
    @DeleteMapping(value = "/note/{id}")
    public Mono<Void> deleteNote(@PathVariable String id) {
        return controllerManager.getNoteController().deleteNote(id);
    }

    /**
     * Used for update the text / date or author of the note
     *
     * @param note new note
     * @return The note updated
     */
    @PutMapping(value = "/note", consumes = APPLICATION_JSON_VALUE)
    public Mono<Note> updateNote(@RequestBody NoteDTO note) {
        return controllerManager.getNoteController().updateNote(note);
    }

}
