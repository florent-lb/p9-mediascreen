package com.ffdc.mediascreen.note.controller.impl;

import com.ffdc.mediascreen.note.model.dto.NoteDTO;
import com.ffdc.mediascreen.note.model.entities.Note;
import com.ffdc.mediascreen.note.model.repositories.NoteRepository;
import com.ffdc.mediascreen.note.controller.contract.NoteController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

@Controller
@Slf4j
public class NoteControllerImpl implements NoteController {

    private final NoteRepository repository;

    public NoteControllerImpl(NoteRepository repository) {
        this.repository = repository;
    }


    @Override
    public Flux<Note> getNoteByPatientId(String idUser) {

        return repository.findByPatientId(idUser);
    }

    @Override
    public Mono<Note> createNote(@Valid NoteDTO note) {

        return Mono.fromSupplier(() ->
                {
                    Note toCreate = new Note();
                    toCreate.setAuthor(note.getAuthor());
                    toCreate.setDateOfInput(note.getDateOfInput());
                    toCreate.setPatientId(note.getPatientId());
                    toCreate.setText(note.getText());
                    return toCreate;
                }
        ).flatMap(repository::insert)
                .onErrorMap(throwable ->
                {
                    if (throwable instanceof ConstraintViolationException) {
                        throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, throwable.getMessage(), throwable);
                    } else {
                        return throwable;
                    }
                });
    }

    @Override
    public Mono<Void> deleteNote(@NotEmpty String id) {
        return repository.deleteById(id);
    }

    @Override
    public Mono<Note> updateNote(@Valid NoteDTO note) {

        return repository.findById(note.getId())
                .flatMap(oldNote ->
                {
                    oldNote.setText(note.getText());
                    oldNote.setAuthor(note.getAuthor());
                    oldNote.setDateOfInput(note.getDateOfInput());
                    return repository.save(oldNote);
                })
                .switchIfEmpty(Mono.error(new NullPointerException("Note unknown for id " + note.getId())))
                .onErrorMap(throwable ->
                {
                    throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, throwable.getMessage(), throwable);
                });
    }

}
