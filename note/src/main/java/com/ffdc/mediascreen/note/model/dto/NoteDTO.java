package com.ffdc.mediascreen.note.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class NoteDTO implements Serializable {

    private String id;

    @NotBlank
    private String patientId;
    @NotBlank
    private String text;

    private LocalDateTime dateOfInput;

    private String author;



}
