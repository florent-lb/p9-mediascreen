package com.ffdc.mediascreen.note.controller.contract;

import com.ffdc.mediascreen.note.model.dto.NoteDTO;
import com.ffdc.mediascreen.note.model.entities.Note;
import org.aspectj.weaver.ast.Not;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface NoteController
{

    Flux<Note> getNoteByPatientId(String idUser);

    Mono<Note> createNote(NoteDTO note);

    Mono<Void> deleteNote(String id);

    Mono<Note> updateNote(NoteDTO note);
}
