package com.ffdc.mediascreen.note.controller.contract;

public interface ControllerManager {

   NoteController getNoteController();

}
