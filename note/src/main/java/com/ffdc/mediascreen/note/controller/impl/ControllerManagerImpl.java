package com.ffdc.mediascreen.note.controller.impl;

import com.ffdc.mediascreen.note.controller.contract.NoteController;
import com.ffdc.mediascreen.note.controller.contract.ControllerManager;
import com.ffdc.mediascreen.note.model.entities.Note;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;

import javax.validation.Valid;

@Controller
public class ControllerManagerImpl implements ControllerManager {

    @Getter
    @Autowired
    NoteController noteController;


}
