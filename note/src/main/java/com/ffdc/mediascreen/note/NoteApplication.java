package com.ffdc.mediascreen.note;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication(scanBasePackages = "com.ffdc.mediascreen")
@EnableWebFlux
@EnableReactiveMongoRepositories(basePackages = "com.ffdc.mediascreen.note.model.repositories")
public class NoteApplication {


    public static void main(String[] args) {
        SpringApplication.run(NoteApplication.class, args);
    }


}
