package com.ffdc.mediascreen.patient;

import com.ffdc.mediascreen.patient.controller.impl.ControllerManagerImpl;
import com.ffdc.mediascreen.patient.controller.impl.PatientControllerImpl;
import com.ffdc.mediascreen.patient.model.dto.PatientDTO;
import com.ffdc.mediascreen.patient.model.entities.Patient;
import com.ffdc.mediascreen.patient.model.repositories.PatientRepository;
import com.ffdc.mediascreen.patient.view.PatientRestController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = PatientRestController.class)
@Import({ControllerManagerImpl.class, PatientControllerImpl.class})
class PatientApplicationTests {


    @MockBean
    PatientRepository repository;

    @Autowired
    private WebTestClient webTestClient;

    private static Patient p1;
    private static Patient p2;

    private static PatientDTO p3;

    @BeforeAll
    public static void setup() {
        p1 = new Patient();
        p1.setAddress("Tours");
        p1.setBirthDate(LocalDate.now().minusYears(20));
        p1.setId("1");
        p1.setFirstName("Jon");
        p1.setLastName("Doe");
        p1.setPhone("666");

        p2 = new Patient();
        p2.setAddress("Srout");
        p2.setBirthDate(LocalDate.now().minusYears(40));
        p2.setId("2");
        p2.setFirstName("Jony");
        p2.setLastName("Cash");
        p2.setPhone("999");

        p3 = new PatientDTO();
        p3.setAddress("Tours");
        p3.setBirthDate(java.sql.Date.valueOf(LocalDate.now().minusYears(20)));
        p3.setId("1");
        p3.setFirstName("Jon");
        p3.setLastName("Doe");
        p3.setPhone("666");

    }

    @Test
    @DisplayName("List all user")
    public void get_whenaskAllUser_ShouldReturnList() {
        Flux<Patient> fluxReturn = Flux.just(p1, p2);
        when(repository.findAll()).thenReturn(fluxReturn);

        webTestClient.get()
                .uri("/list")
                .exchange()
                .expectStatus()
                .is2xxSuccessful().expectBodyList(PatientDTO.class);


        verify(repository, Mockito.times(1)).findAll();

    }

    @Test
    @DisplayName("Create a user")
    public void get_addUser_ShouldReturnCreated() {
        when(repository.insert(any(Patient.class))).thenReturn(Mono.just(p1));

        webTestClient.post()
                .uri("/patient")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(p1))
                .exchange()
                .expectStatus()
                .isCreated().expectBody(PatientDTO.class);


        verify(repository, Mockito.times(1)).insert(p1);

    }

    @Test
    @DisplayName("Search by Id")
    public void get_whenaskOneUser_ShouldReturnit() {
        when(repository.findById(p1.getId())).thenReturn(Mono.just(p1));

        webTestClient.get()
                .uri("/patient/" + p1.getId())
                .exchange()
                .expectStatus()
                .is2xxSuccessful().expectBody(PatientDTO.class);


        verify(repository, Mockito.times(1)).findById(p1.getId());

    }

    @Test
    @DisplayName("Update user")
    public void get_whenaskUpdateUser_ShouldReturnItUpdated() {

        Patient p1New = new Patient();
        p1New.setId(p1.getId());
        p1New.setAddress("AAAA");


        when(repository.existsById(any(String.class))).thenReturn(Mono.just(true));
        when(repository.save(any())).thenReturn(Mono.just(p1New));




        webTestClient.post()
                .uri("/patient/update")
                .body(BodyInserters.fromValue(p1New))
                .exchange()
                .expectStatus()
                .is2xxSuccessful()
                .expectBody(PatientDTO.class)
                .value(patientDto -> Assertions.assertThat(patientDto.getAddress()).isEqualTo(p1New.getAddress()));


        verify(repository, Mockito.times(1)).existsById(p1.getId());

    }

    @Test
    @DisplayName("Update with wrong user")
    public void get_whenaskUpdateWrongUser_ShouldReturnNok() {
        when(repository.existsById(any(String.class))).thenReturn(Mono.just(false));

        webTestClient.post()
                .uri("/patient/update")
                .body(BodyInserters.fromValue(p1))
                .exchange()
                .expectStatus()
                .is4xxClientError();


        verify(repository, Mockito.times(1)).existsById(p1.getId());

    }

    @Test
    @DisplayName("Search by Last Name")
    public void get_whenaskUpdateByLastName_ShouldReturnFlux() {
        when(repository.findByLastNameContaining(any(String.class))).thenReturn(Flux.just(p1));

        webTestClient.get()
                .uri("/patient/search/"+p1.getLastName())
                .exchange()
                .expectStatus()
                .is2xxSuccessful().expectBodyList(PatientDTO.class);


        verify(repository, Mockito.times(1)).findByLastNameContaining(any(String.class));

    }

}
