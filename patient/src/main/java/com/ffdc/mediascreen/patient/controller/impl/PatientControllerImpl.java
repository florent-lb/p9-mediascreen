package com.ffdc.mediascreen.patient.controller.impl;

import com.ffdc.mediascreen.patient.model.dto.PatientDTO;
import com.ffdc.mediascreen.patient.model.dto.PatientDTOConverter;
import com.ffdc.mediascreen.patient.model.repositories.PatientRepository;
import com.ffdc.mediascreen.patient.controller.contract.PatientController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Controller
@Slf4j
public class PatientControllerImpl implements PatientController {

    private final PatientRepository repository;

    public PatientControllerImpl(PatientRepository repository) {
        this.repository = repository;
    }


    @Override
    public Flux<PatientDTO> getAllPatients() {
        return repository.findAll().map(PatientDTOConverter::getPatientDTO);
    }

    @Override
    public Mono<PatientDTO> addPatient(PatientDTO patient) {
        return repository.insert(PatientDTOConverter.getPatient(patient)).map(PatientDTOConverter::getPatientDTO)
                .doOnSuccess(patientDTO -> logger.info("Create Patient : " + patientDTO))
                .doOnError(throwable -> logger.error("Impossible to create Patient : \n" + patient,throwable))
                ;
    }

    @Override
    public Mono<PatientDTO> getById(String id) {
        return repository.findById(id).map(PatientDTOConverter::getPatientDTO);
    }

    @Override
    public Mono<PatientDTO> updatePatient(PatientDTO patient) {
        return repository.existsById(patient.getId())
                .flatMap(ifExist -> {
                    if (ifExist) {

                        return repository.save(PatientDTOConverter.getPatient(patient))
                                .doOnSuccess(patientBdd -> logger.info("Update Patient : " + patientBdd.toString()))
                                .map(PatientDTOConverter::getPatientDTO);
                    } else {
                        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "No user found"));
                    }
                });

    }

    @Override
    public Flux<PatientDTO> searchPatientByName(String lastName) {
        return repository
                .findByLastNameContaining(lastName).map(PatientDTOConverter::getPatientDTO);
    }
}
