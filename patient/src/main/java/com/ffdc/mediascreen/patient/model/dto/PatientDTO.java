package com.ffdc.mediascreen.patient.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PatientDTO {

    private String id;

    private String lastName;

    private String firstName;

    private Date birthDate;

    private String address;

    private String phone;

    private char sex;
}
