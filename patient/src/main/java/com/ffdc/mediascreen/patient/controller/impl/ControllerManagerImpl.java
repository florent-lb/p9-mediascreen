package com.ffdc.mediascreen.patient.controller.impl;

import com.ffdc.mediascreen.patient.controller.contract.PatientController;
import com.ffdc.mediascreen.patient.controller.contract.ControllerManager;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ControllerManagerImpl implements ControllerManager {

    @Getter
    @Autowired
    PatientController patientController;


}
