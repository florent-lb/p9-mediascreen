package com.ffdc.mediascreen.patient.model.repositories;

import com.ffdc.mediascreen.patient.model.entities.Patient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Flux;

@Configuration
public interface PatientRepository extends ReactiveMongoRepository<Patient,String> {

    Flux<Patient> findByLastNameContaining(String lastName);
}
