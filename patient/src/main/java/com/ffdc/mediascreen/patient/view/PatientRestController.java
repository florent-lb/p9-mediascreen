package com.ffdc.mediascreen.patient.view;

import com.ffdc.mediascreen.patient.model.dto.PatientDTO;
import com.ffdc.mediascreen.patient.model.entities.Patient;
import com.ffdc.mediascreen.patient.controller.contract.ControllerManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.UncategorizedMongoDbException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ServerErrorException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
public class PatientRestController {

    private final ControllerManager controllerManager;


    public PatientRestController(ControllerManager controllerManager) {
        this.controllerManager = controllerManager;
    }

    /**
     * @return A Flux with patients in database
     */
    @GetMapping(value = "/list", produces = APPLICATION_JSON_VALUE)
    public Flux<PatientDTO> getAllPatients() {
        return controllerManager.getPatientController()
                .getAllPatients()
                .onErrorResume(throwable ->
                        Flux.error(
                                Optional.of(throwable).
                                        filter(e -> e instanceof UncategorizedMongoDbException)
                                        .map(e -> new ServerErrorException("Impossible to reach the database", e))
                                        .orElseGet(() -> new ServerErrorException("Impossible to reach the database", throwable)))
                );
    }

    /**
     * Search one patient by his id
     * @param id technical id of the user in BDD
     * @return The patient if exist
     */
    @GetMapping(value = "/patient/{id}", produces = APPLICATION_JSON_VALUE)
    public Mono<PatientDTO> getById(@PathVariable String id) {
        return controllerManager.getPatientController().getById(id);
    }

    /**
     * Create a Patient
     * @param patient Json Patient without id
     * @return The Patient created with his id
     */
    @PostMapping(value = "/patient", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<Mono<PatientDTO>> addPatient(@RequestBody PatientDTO patient) {

        return ResponseEntity
                .created(UriComponentsBuilder.newInstance().pathSegment("/patient/").build().toUri())
                .body(controllerManager
                        .getPatientController()
                        .addPatient(patient));
    }

    /**
     * Save the new state of the patient
     * NOTE : the id must not be null !
     * @param patient Patient with his ID
     * @return The updated patient
     */
    @PostMapping(value = "/patient/update", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Mono<PatientDTO> updatePatient(@RequestBody PatientDTO patient) {

        return controllerManager
                .getPatientController()
                .updatePatient(patient);


    }

    /**
     * Search list of patient by Name
     * @param lastName the last name or a part of it
     * @return The patients searched
     */
    @GetMapping(value = "/patient/search/{lastName}", produces = APPLICATION_JSON_VALUE)
    public Flux<PatientDTO> searchPatientByName(@PathVariable String lastName)
    {
        return controllerManager
                .getPatientController()
                .searchPatientByName(lastName);
    }

}
