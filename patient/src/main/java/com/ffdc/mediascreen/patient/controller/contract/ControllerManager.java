package com.ffdc.mediascreen.patient.controller.contract;

public interface ControllerManager {

   PatientController getPatientController();

}
