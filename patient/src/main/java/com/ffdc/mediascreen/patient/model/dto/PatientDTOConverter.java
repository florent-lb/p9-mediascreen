package com.ffdc.mediascreen.patient.model.dto;

import com.ffdc.mediascreen.patient.model.entities.Patient;
import lombok.experimental.UtilityClass;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

@UtilityClass
public class PatientDTOConverter {

    public static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE;

    public PatientDTO getPatientDTO(Patient patient) {
        return PatientDTO.builder()
                .address(patient.getAddress())
                .birthDate(Optional.ofNullable(patient.getBirthDate()).map(java.sql.Date::valueOf).orElse(null))
                .firstName(patient.getFirstName())
                .lastName(patient.getLastName())
                .phone(patient.getPhone())
                .sex(patient.getSex())
                .id(patient.getId())
                .build();
    }

    public Patient getPatient(PatientDTO patientDTO) {
        return Patient.builder()
                .address(patientDTO.getAddress())
                .birthDate(Optional.ofNullable(patientDTO.getBirthDate())
                        .map(Date::toInstant)
                        .map(instant -> instant.atZone(ZoneId.systemDefault()))
                        .map(ZonedDateTime::toLocalDate).orElse(null)                       )
                .firstName(patientDTO.getFirstName())
                .lastName(patientDTO.getLastName())
                .phone(patientDTO.getPhone())
                .sex(patientDTO.getSex())
                .id(patientDTO.getId())
                .build();

    }


}
