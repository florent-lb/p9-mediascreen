package com.ffdc.mediascreen.patient.controller.contract;

import com.ffdc.mediascreen.patient.model.dto.PatientDTO;
import com.ffdc.mediascreen.patient.model.entities.Patient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PatientController
{
    Flux<PatientDTO> getAllPatients();

    Mono<PatientDTO> addPatient(PatientDTO patient);

    Mono<PatientDTO> getById(String id);

    Mono<PatientDTO> updatePatient(PatientDTO patient);

    Flux<PatientDTO> searchPatientByName(String name);

}
