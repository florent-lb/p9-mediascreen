package com.ffdc.mediascreen.patient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.config.EnableWebFlux;

@SpringBootApplication(scanBasePackages = "com.ffdc.mediascreen")
@EnableWebFlux
@EnableReactiveMongoRepositories(basePackages = "com.ffdc.mediascreen.patient.model.repositories")
public class PatientApplication {


    public static void main(String[] args) {
        SpringApplication.run(PatientApplication.class, args);
    }


}
